#!/bin/bash

set -e

# Stop the container
if docker ps | grep etworker >/dev/null 2>/dev/null; then
    docker kill etworker
fi

# Remove the container
if docker ps -a | grep etworker >/dev/null 2>&1; then
    docker rm etworker
fi

# Update the image
docker pull einsteintoolkit/et-jenkins-worker:ubuntu-20.04

# Restart the container with the new image
./run.sh

echo "Jenkins build worker successfully updated"
