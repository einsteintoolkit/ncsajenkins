set -e
set -x

sudo bash -c "echo 127.1.0.1 $(hostname) >> /etc/hosts"

sudo apt-get update
sudo apt-get dist-upgrade -y
sudo apt-get install -y docker.io emacs-nox
sudo gpasswd -a ${USER} docker
newgrp docker<<EOF
./run.sh
EOF

#sudo bash -c 'echo 1 > /proc/sys/net/ipv4/tcp_mtu_probing'
